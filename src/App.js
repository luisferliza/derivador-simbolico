import React from 'react';
import './assets/css/App.css';
import Form from './components/Form';

function App() {
  return (
    <div className="App">
      <h1>Derivador Simbólico</h1>
      <Form />
    </div>
  );
}

export default App;
