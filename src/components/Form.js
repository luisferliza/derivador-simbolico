import '../assets/css/Form.css'
import React, { Component } from 'react'
import { xsltProcess, xmlParse } from 'xslt-processor'
import beautify  from 'xml-beautifier';

export class Form extends Component {


    constructor(props) {
        super(props)

        this.state = {
            input: '',
            output: '',
            transformation: '',
        }

        fetch('/transformation.xsl')
            .then((r) => r.text())
            .then(text => {
                this.setState({
                    transformation: text
                })
            })

        fetch('/example.xml')
            .then((r) => r.text())
            .then(text => {
                this.setState({
                    input: text
                })
            })
    }

    handleXMLTransformation = (event) => {
        this.setState({
            input: event.target.value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        this.setState({
            output: xsltProcess(
                xmlParse(`<?xml version="1.0"?>        
                <?xml-stylesheet type="text/xsl" href="Transformation.xsl"?>
                ${this.state.input}`),
                xmlParse(this.state.transformation)
            )

        })
    }
    handleFormat = event => {
        event.preventDefault();
        this.setState({
            output: beautify(this.state.output),
            input: beautify(this.state.input)
        })
    }

    render() {

        return (
            <form className='Formdiv'>

                <div>
                    <table>
                        <tbody>
                            <tr>
                                <th align="left">Ingrese la entrada deseada:</th>
                                <th align="left">Salida:</th>
                            </tr>
                            <tr>
                                <td>
                                    <textarea
                                        rows="20"
                                        type="text"
                                        value={this.state.input}
                                        onChange={this.handleXMLTransformation}
                                    ></textarea>
                                </td>
                                <td>
                                    <textarea
                                        rows="20"
                                        type="text"
                                        value={this.state.output}
                                        readOnly={true}
                                    ></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <button type="submit" onClick={this.handleSubmit}>Convertir</button>
                    <button type="submit" onClick={this.handleFormat}>Dar Formato</button>
                </div>
            </form>
        )
    }
}

export default Form
