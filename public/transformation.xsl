<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="f">
        <g>
            <xsl:apply-templates />
        </g>
    </xsl:template>
    
    <xsl:template match="plus">
        <plus>                  
            <xsl:apply-templates />
        </plus>
    </xsl:template>
    
    <xsl:template match="minus">
        <minus>                  
            <xsl:apply-templates />
        </minus>
    </xsl:template>
    
    <xsl:template match="times">
        <plus>
            <times>
                <xsl:apply-templates select="*[1]"/>      
                <xsl:copy-of select="*[2]"/>                
            </times>
            <times>
                <xsl:copy-of select="*[1]"/>
                <xsl:apply-templates select="*[2]"/>      
            </times>            
        </plus>        
    </xsl:template>
    
    <xsl:template match="division">
        <divide>
            <minus>
                <times>
                    <xsl:apply-templates select="*[1]"/>      
                    <xsl:copy-of select="*[2]"/>                
                </times>
                <times>
                    <xsl:apply-templates select="*[2]"/>      
                    <xsl:copy-of select="*[1]"/>    
                </times>            
            </minus>     
            <power>
                <xsl:copy-of select="*[2]"/>                
                <const>2</const>
            </power>
        </divide>   
    </xsl:template>
    
    <xsl:template match="power">
        <times>
            <times>                
                <xsl:copy-of select="*[2]"/>                
                <power>
                    <xsl:copy-of select="*[1]"/>
                    <const> 
                        <xsl:value-of select="*[2] - 1"/> 
                    </const>                
                </power>            
            </times>
            <xsl:apply-templates select="*[1]"/>   
        </times>      
    </xsl:template>
    
    <xsl:template match="var">
        <const>1</const>
    </xsl:template>
    
    <xsl:template match="const">
        <const>0</const>
    </xsl:template>    

</xsl:stylesheet>
